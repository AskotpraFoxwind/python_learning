
jmeno = "Karel"
pozdrav_raw = "Ahoj " + jmeno
pozdrav_format = f"Ahoj {jmeno} tohle je z formatovaneho text take 2 + 2 jsou {2+2}"
# uvnitr slozenych zavorek je mozne psat python kod a vystup je vlozen do textu na urcenou pozici

print(pozdrav_format)


# Formatovani Cisla
cislo = 3.22222222222222222
print(cislo)
print(f"{cislo:.3}") # vezmi urcity pocet desetinnych mist
jine_cislo = 10
print(f"{jine_cislo:05}") # zarovnani cisla pridanim nul

