

"""
__________________________________________________PRIORITA 1
ARITMETICKE OPERATORY

+	SCITANI
-	ODCITANI
*	NASOBENI
/	DELENI
//	CELOCISELNE DELENI	{ZAOKROUHLENI VZDY DOLU}
**	UMOCNIVANI
%	MODULO {VYSLEDEK JE ZBBYTEK}

__________________________________________________PRIORITA 2
POROVNAVACI OPERATORY

< 	MENSI NEZ
>	VETSI NEZ
==	ROVNA SE
<=	MENSI NEBO ROVNO
>=	VETSI NEBO ROVNO

__________________________________________________PRIORITA 3
LOGICKE OPERATORY

AND				"OBOJI" JE TRUE
1 + 0 => 0
0 + 1 => 0
1 + 1 => 1
0 + 0 => 0


OR				ALESPON JEDNO JE TRUE
1 + 0 => 1
0 + 1 => 1
1 + 1 => 1
0 + 0 => 0

NOT				"PROHODI" 	TRUE => FALSE	
1 => 0						FALSE => TRUE
0 => 1

"""
