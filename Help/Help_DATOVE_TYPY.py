
"""
DATOVE TYPY
__________________________________________________
STRING	- TEXT

print("ahoj"[1])			pracovani s pismeny
print("10" + "15")
print("ah" + "hoj}

__________________________________________________
INTEGER	- CELE CISLO

print(10 + 25)
print(123456)
print(123_456)

__________________________________________________
FLOAT - DESETINNE CISLO

print(3.14)
print(0.154325)

__________________________________________________
BOLEAN - PRAVDA / NEPRAVDA	

True
False

__________________________________________________


"""