# Zobrazit stav 
`git status`

# Zobrazit historii zmen
`git log`

# Pridat soubor do zmeny
`git add [cesta k souboru|. (pridat vse)|-p vybrat rucne po castech]`

# Ulozit zmenu
`git commit -m "Strucny popis zmeny priklad: Pridani noveho tlacitka"`


# Nahrani na server
`git push` + heslo od ssh klice

